# Maîtrise de poste

## I. Self-footprinting

## Host OS

Pour déterminer les principales informations de notre machine il faut taper ```sudo lshw```

### Output

```console
[...]

agrorec@agrorec-Aspire-F5-573G:~$ sudo lshw
agrorec-aspire-f5-573g      
    description: Notebook
    product: Aspire F5-573G (Aspire F5-573G_108F_1.04)
    vendor: Acer
    version: V1.04
    serial: NXGFGEF00263012C907600
    width: 64 bits
    capabilities: smbios-3.0 dmi-3.0 smp vsyscall32
    configuration: chassis=notebook family=SKL sku=Aspire F5-573G_108F_1.04 uuid=DED9D69D-0CF5-4ACB-8495-54AB3AB320CE
  *-core
       description: Motherboard
       product: Captain_SK
       vendor: Acer
       physical id: 0
       version: V1.04
       serial: NBGDW1100463012C907600
       slot: Type2 - Board Chassis Location
     *-firmware
          description: BIOS
          vendor: Insyde Corp.
          physical id: 0
          version: V1.04
          date: 04/26/2016
          size: 128KiB
          capacity: 4544KiB
          capabilities: pci upgrade shadowing cdboot bootselect edd int13floppynec int13floppytoshiba int13floppy360 int13floppy1200 int13floppy720 int13floppy2880 int9keyboard int10video acpi usb biosbootspecification uefi
     *-cpu
          description: CPU
          product: Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
          vendor: Intel Corp.
          physical id: 4
          bus info: cpu@0
          version: Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
          serial: To Be Filled By O.E.M.
          slot: U3E1
          size: 1473MHz
          capacity: 4005MHz
          width: 64 bits
          clock: 100MHz
          capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp cpufreq
          configuration: cores=2 enabledcores=2 threads=4
[...]
```
***

## Devices

### Trouver

Pour trouver la marque et le modèle du processeur, j'ai utilisé la commande ```sudo lshw -class processor``` avec laquelle on peut aussi voir le nombre de processeurs et de coeurs

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ sudo lshw -class processor
  *-cpu                     
       description: CPU
       product: Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
       vendor: Intel Corp.
       physical id: 4
       bus info: cpu@0
       version: Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
       serial: To Be Filled By O.E.M.
       slot: U3E1
       size: 998MHz
       capacity: 4005MHz
       width: 64 bits
       clock: 100MHz
       capabilities: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch cpuid_fault epb invpcid_single pti tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt intel_pt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp cpufreq
       configuration: cores=2 enabledcores=2 threads=4
``` 
#

Le nom du processeur est Intel(R) Core(TM) i5-6200U CPU @ 2.30 GHz.

* Intel(R) Core(TM) correspond au nom du processeur : Core est donc leur modèle plus performant ;
* i5 correspond aux catégories (plus le nombre est haut, plus les performances sont élevées) ;
* 62 correspond à la génération du processeur, ici la sixième ;
* 00 correspond au SKU (Stock Keeping Unit ou Unité de gestion des stocks) qui représente une référence unique de produit
* U correspond à la gamme du processeur, ici "Mobile power efficient"

# 

Afin de trouver la marque et le modèle de mon trackpad j'ai utilisé la commande ```xinput list```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ xinput list
⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
⎜   ↳ ELAN0501:00 04F3:3019 Touchpad          	id=12	[slave  pointer  (2)]
⎜   ↳ SteelSeries SteelSeries Sensei Ten Consumer Control	id=15	[slave  pointer  (2)]
⎜   ↳ SteelSeries SteelSeries Sensei Ten      	id=18	[slave  pointer  (2)]
⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
    ↳ Power Button                            	id=6	[slave  keyboard (3)]
    ↳ Video Bus                               	id=7	[slave  keyboard (3)]
    ↳ Video Bus                               	id=8	[slave  keyboard (3)]
    ↳ Power Button                            	id=9	[slave  keyboard (3)]
    ↳ Sleep Button                            	id=10	[slave  keyboard (3)]
    ↳ HD WebCam: HD WebCam                    	id=11	[slave  keyboard (3)]
    ↳ AT Translated Set 2 keyboard            	id=13	[slave  keyboard (3)]
    ↳ Acer WMI hotkeys                        	id=14	[slave  keyboard (3)]
    ↳ SteelSeries SteelSeries Sensei Ten Consumer Control	id=16	[slave  keyboard (3)]
    ↳ SteelSeries SteelSeries Sensei Ten Keyboard	id=17	[slave  keyboard (3)]
```
#

Pour trouver la marque et le modèle de ma carte graphique j'ai utilisé la commande ```sudo lshw -C video```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ sudo lshw -C video
[sudo] password for agrorec:     
  *-display                 
       description: VGA compatible controller
       product: Skylake GT2 [HD Graphics 520]
       vendor: Intel Corporation
       physical id: 2
       bus info: pci@0000:00:02.0
       version: 07
       width: 64 bits
       clock: 33MHz
       capabilities: pciexpress msi pm vga_controller bus_master cap_list rom
       configuration: driver=i915 latency=0
       resources: irq:131 memory:92000000-92ffffff memory:a0000000-afffffff ioport:5000(size=64) memory:c0000-dffff
  *-display
       description: 3D controller
       product: NVIDIA Corporation
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:01:00.0
       version: a2
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress bus_master cap_list rom
       configuration: driver=nouveau latency=0
       resources: irq:130 memory:93000000-93ffffff memory:80000000-8fffffff memory:90000000-91ffffff ioport:4000(size=128) memory:94080000-940fffff
```
***

### Disque dur
Pour identifier la marque et le modèle de mon disque dur j'ai utilisé la commande ```sudo lshw -class disk```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ sudo lshw -class disk
  *-cdrom                   
       description: DVD-RAM writer
       product: DVD A  DA8AESH
       vendor: Slimtype
       physical id: 0
       bus info: scsi@1:0.0.0
       logical name: /dev/cdrom
       logical name: /dev/cdrw
       logical name: /dev/dvd
       logical name: /dev/dvdrw
       logical name: /dev/sr0
       version: XA11
       capabilities: removable audio cd-r cd-rw dvd dvd-r dvd-ram
       configuration: ansiversion=5 status=nodisc
  *-disk
       description: ATA Disk
       product: HFS256G39TND-N21
       physical id: 1
       bus info: scsi@2:0.0.0
       logical name: /dev/sda
       version: 1P10
       serial: FI67N011310607A54
       size: 238GiB (256GB)
       capabilities: partitioned partitioned:dos
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=a9c88e6c
```

# 

Pour identifier les différentes parties du disque dur j'ai utilisé la commande ```fdisk -l```

```console
agrorec@agrorec-Aspire-F5-573G:~$ sudo fdisk -l
Disk /dev/sda: 238,49 GiB, 256060514304 bytes, 500118192 sectors
Disk model: HFS256G39TND-N21
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0xa9c88e6c

Device     Boot Start       End   Sectors   Size Id Type
/dev/sda1  *     2048 500117503 500115456 238,5G 83 Linux
#### Output

```

# 

Pour déterminer le système de fichier de chaque partition j'ai utilisé la commande ```df -Th```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ df -Th
Filesystem     Type      Size  Used Avail Use% Mounted on
udev           devtmpfs  3,8G     0  3,8G   0% /dev
tmpfs          tmpfs     784M  1,6M  783M   1% /run
/dev/sda1      ext4      234G   35G  188G  16% /
tmpfs          tmpfs     3,9G   35M  3,8G   1% /dev/shm
tmpfs          tmpfs     5,0M  4,0K  5,0M   1% /run/lock
tmpfs          tmpfs     3,9G     0  3,9G   0% /sys/fs/cgroup
tmpfs          tmpfs     784M   24K  784M   1% /run/user/1000
```

***


## Users

Pour déterminer la liste complète des utilisateurs de la machine j'utilise la commande ``` cat /etc/passwd```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-network:x:100:102:systemd Network Management,,,:/run/systemd/netif:/usr/sbin/nologin
systemd-resolve:x:101:103:systemd Resolver,,,:/run/systemd/resolve:/usr/sbin/nologin
syslog:x:102:106::/home/syslog:/usr/sbin/nologin
messagebus:x:103:107::/nonexistent:/usr/sbin/nologin
_apt:x:104:65534::/nonexistent:/usr/sbin/nologin
uuidd:x:105:111::/run/uuidd:/usr/sbin/nologin
cups-pk-helper:x:106:112:user for cups-pk-helper service,,,:/home/cups-pk-helper:/usr/sbin/nologin
kernoops:x:107:65534:Kernel Oops Tracking Daemon,,,:/:/usr/sbin/nologin
rtkit:x:108:113:RealtimeKit,,,:/proc:/usr/sbin/nologin
avahi-autoipd:x:109:114:Avahi autoip daemon,,,:/var/lib/avahi-autoipd:/usr/sbin/nologin
usbmux:x:110:46:usbmux daemon,,,:/var/lib/usbmux:/usr/sbin/nologin
systemd-coredump:x:111:117:systemd core dump processing,,,:/run/systemd:/usr/sbin/nologin
ntp:x:112:118::/nonexistent:/usr/sbin/nologin
lightdm:x:113:119:Light Display Manager:/var/lib/lightdm:/bin/false
dnsmasq:x:114:65534:dnsmasq,,,:/var/lib/misc:/usr/sbin/nologin
saned:x:115:122::/var/lib/saned:/usr/sbin/nologin
nm-openvpn:x:116:123:NetworkManager OpenVPN,,,:/var/lib/openvpn/chroot:/usr/sbin/nologin
avahi:x:117:124:Avahi mDNS daemon,,,:/var/run/avahi-daemon:/usr/sbin/nologin
colord:x:118:125:colord colour management daemon,,,:/var/lib/colord:/usr/sbin/nologin
speech-dispatcher:x:119:29:Speech Dispatcher,,,:/var/run/speech-dispatcher:/bin/false
pulse:x:120:126:PulseAudio daemon,,,:/var/run/pulse:/usr/sbin/nologin
hplip:x:121:7:HPLIP system user,,,:/var/run/hplip:/bin/false
geoclue:x:122:128::/var/lib/geoclue:/usr/sbin/nologin
agrorec:x:1000:1000:Agrorec:/home/agrorec:/bin/bash
tcpdump:x:123:131::/nonexistent:/usr/sbin/nologin
tss:x:124:132:TPM software stack,,,:/var/lib/tpm:/bin/false
_flatpak:x:125:135:Flatpak system-wide installation helper,,,:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:999:999:systemd Time Synchronization:/:/usr/sbin/nologin
nvidia-persistenced:x:126:136:NVIDIA Persistence Daemon,,,:/nonexistent:/usr/sbin/nologin
```

# 

Pour trouver le nom de l'utilisateur qui est full admin sur la machine j'ai utilisé la commande ``` getent group sudo | cut -d: -f4```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ getent group sudo | cut -d: -f4
agrorec
```
***

## Processus

Pour déterminer la liste des processus de la machine j'utilise la commande ``` ps -aux | less```

### Output 


```console
 [...]
root         193  0.0  0.0      0     0 ?        I<   Oct15   0:00 [scsi_tmf_2]
root         194  0.0  0.0      0     0 ?        S    Oct15   0:26 [irq/82-ELAN0501]
root         196  0.0  0.0      0     0 ?        I<   Oct15   0:00 [cryptd]
root         217  0.0  0.0      0     0 ?        I<   Oct15   0:00 [kworker/1:1H-kblockd]
root         222  0.0  0.0      0     0 ?        I<   Oct15   0:00 [kworker/3:1H-kblockd]
root         234  0.0  0.0      0     0 ?        I<   Oct15   0:00 [kworker/2:1H-kblockd]
root         283  0.0  0.0      0     0 ?        S    Oct15   0:01 [jbd2/sda1-8]
root         284  0.0  0.0      0     0 ?        I<   Oct15   0:00 [ext4-rsv-conver]
root         285  0.0  0.0      0     0 ?        I<   Oct15   0:00 [kworker/0:1H-kblockd]
root         329  0.0  0.4  72484 38128 ?        S<s  Oct15   0:05 /lib/systemd/systemd-journald
root         360  0.0  0.0  23536  7212 ?        Ss   Oct15   0:01 /lib/systemd/systemd-udevd
systemd+     487  0.0  0.1  24716 14112 ?        Ss   Oct15   0:07 /lib/systemd/systemd-resolved
root         489  0.0  0.0 238492  7552 ?        Ssl  Oct15   0:01 /usr/lib/accountsservice/accounts-daemon
root         490  0.0  0.0   2540   848 ?        Ss   Oct15   0:00 /usr/sbin/acpid
avahi        491  0.0  0.0   8684  3756 ?        Ss   Oct15   0:00 avahi-daemon: running [agrorec-Aspire-F5-573G.local]
root         492  0.0  0.0   9640  3264 ?        Ss   Oct15   0:00 /usr/sbin/cron -f
message+     494  0.0  0.0   9156  6136 ?        Ss   Oct15   0:07 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
root         496  0.0  0.2 483768 20612 ?        Ssl  Oct15   0:14 /usr/sbin/NetworkManager --no-daemon
root         504  0.0  0.0  81880  3548 ?        Ssl  Oct15   0:04 /usr/sbin/irqbalance --foreground
root         509  0.0  0.2  39528 20436 ?        Ss   Oct15   0:00 /usr/bin/python3 /usr/bin/networkd-dispatcher --run-startup-triggers
root         512  0.0  0.1 241408 10988 ?        Ssl  Oct15   0:00 /usr/lib/policykit-1/polkitd --no-debug
syslog       514  0.0  0.0 224532  5204 ?        Ssl  Oct15   0:01 /usr/sbin/rsyslogd -n -iNONE
root         519  0.0  0.1  17168  8172 ?        Ss   Oct15   0:00 /lib/systemd/systemd-logind
root         520  0.0  0.1 125852  9752 ?        Ssl  Oct15   0:01 /usr/sbin/thermald --no-daemon --dbus-enable
root         527  0.0  0.1 392876 12968 ?        Ssl  Oct15   0:05 /usr/lib/udisks2/udisksd
root         528  0.0  0.1  14060  9252 ?        Ss   Oct15   0:05 /sbin/wpa_supplicant -u -s -O /run/wpa_supplicant
avahi        531  0.0  0.0   8552   352 ?        S    Oct15   0:00 avahi-daemon: chroot helper
root         561  0.0  0.1 240032 10688 ?        Ssl  Oct15   0:00 /usr/sbin/ModemManager --filter-policy=strict
root         604  0.0  0.0 306240  7476 ?        SLsl Oct15   0:00 /usr/sbin/lightdm
ntp          607  0.0  0.0  74980  4432 ?        Ssl  Oct15   0:08 /usr/sbin/ntpd -p /var/run/ntpd.pid -g -u 112:118
root         622  0.1  1.5 1023504 122400 tty7   Ssl+ Oct15   7:03 /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
root         624  0.0  0.0   8656  1936 tty1     Ss+  Oct15   0:00 /sbin/agetty -o -p -- \u --noclear tty1 linux
root         665  0.0  0.0      0     0 ?        I<   Oct15   0:00 [kmemstick]
root         690  0.0  0.0      0     0 ?        I<   Oct15   0:00 [cfg80211]
root         723  0.0  0.0      0     0 ?        S    Oct15   0:00 [nv_queue]
root         724  0.0  0.0      0     0 ?        S    Oct15   0:00 [nv_queue]
root         728  0.0  0.0      0     0 ?        I<   Oct15   0:00 [ath10k_wq]
[...]
```
 

# 

Pour déterminer les processus lancés par l'utilisateur full admin (ici agrorec) j'ai utilisé la commande ```ps -fU agrorec```

### Output

```console
agrorec@agrorec-Aspire-F5-573G:~$ ps -fU agrorec
UID          PID    PPID  C STIME TTY          TIME CMD
agrorec     2576       1  0 17:05 ?        00:00:00 /lib/systemd/systemd --user
agrorec     2577    2576  0 17:05 ?        00:00:00 (sd-pam)
agrorec     2586    2576  0 17:05 ?        00:00:01 /usr/bin/pulseaudio --daemon
agrorec     2589       1  0 17:05 ?        00:00:00 /usr/bin/gnome-keyring-daemo
agrorec     2592    1059  0 17:05 ?        00:00:00 xfce4-session
agrorec     2607    2576  0 17:05 ?        00:00:02 /usr/bin/dbus-daemon --sessi
agrorec     2664    2592  0 17:05 ?        00:00:00 /usr/bin/ssh-agent /usr/bin/
agrorec     2682    2576  0 17:05 ?        00:00:00 /usr/libexec/at-spi-bus-laun
agrorec     2687    2682  0 17:05 ?        00:00:00 /usr/bin/dbus-daemon --confi
agrorec     2691    2576  0 17:05 ?        00:00:00 /usr/lib/x86_64-linux-gnu/xf
agrorec     2697    2576  0 17:05 ?        00:00:04 /usr/libexec/at-spi2-registr
agrorec     2703    2576  0 17:05 ?        00:00:00 /usr/bin/gpg-agent --supervi
agrorec     2705    2592  2 17:05 ?        00:01:30 xfwm4
agrorec     2708    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfsd
agrorec     2713    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfsd-fuse /run
agrorec     2728       1  0 17:05 ?        00:00:03 xfsettingsd
agrorec     2729    2592  0 17:05 ?        00:00:09 xfce4-panel
agrorec     2735    2592  0 17:05 ?        00:00:00 Thunar --daemon
agrorec     2740    2592  0 17:05 ?        00:00:02 xfdesktop
agrorec     2743    2729  0 17:05 ?        00:00:00 /usr/lib/x86_64-linux-gnu/xf
agrorec     2744    2729  0 17:05 ?        00:00:00 /usr/lib/x86_64-linux-gnu/xf
agrorec     2745    2729  0 17:05 ?        00:00:00 /usr/lib/x86_64-linux-gnu/xf
agrorec     2748    2729  0 17:05 ?        00:00:04 /usr/lib/x86_64-linux-gnu/xf
agrorec     2751    2729  0 17:05 ?        00:00:01 /usr/lib/x86_64-linux-gnu/xf
agrorec     2754    2729  0 17:05 ?        00:00:06 /usr/lib/x86_64-linux-gnu/xf
agrorec     2766    2576  0 17:05 ?        00:00:00 /usr/lib/x86_64-linux-gnu/xf
agrorec     2769    2592  0 17:05 ?        00:00:11 geary --gapplication-service
agrorec     2778    2592  0 17:05 ?        00:00:00 /usr/bin/python3 /usr/share/
agrorec     2779    2592  0 17:05 ?        00:00:03 nm-applet
agrorec     2788       1  0 17:05 ?        00:00:05 mintreport-tray
agrorec     2795    2592  0 17:05 ?        00:00:00 blueberry-obex-agent
agrorec     2798    2592  0 17:05 ?        00:00:00 /usr/lib/policykit-1-gnome/p
agrorec     2802    2592  0 17:05 ?        00:00:00 /usr/libexec/geoclue-2.0/dem
agrorec     2804       1  0 17:05 ?        00:00:00 xfce4-volumed
agrorec     2809       1  0 17:05 ?        00:00:00 xfce4-power-manager
agrorec     2815    2592  0 17:05 ?        00:00:00 light-locker
agrorec     2818       1  0 17:05 ?        00:00:00 blueberry-tray
agrorec     2819    2592  0 17:05 ?        00:00:00 /usr/libexec/evolution-data-
agrorec     2830       1  0 17:05 ?        00:00:01 mintUpdate
agrorec     2841    2576  0 17:05 ?        00:00:00 /usr/libexec/dconf-service
agrorec     2857    2576  0 17:05 ?        00:00:00 /usr/libexec/evolution-sourc
agrorec     2867    2576  0 17:05 ?        00:00:00 /usr/lib/bluetooth/obexd
agrorec     2887    2576  0 17:05 ?        00:00:00 /usr/bin/gnome-keyring-daemo
agrorec     2904    2576  0 17:05 ?        00:00:00 /usr/libexec/evolution-calen
agrorec     2927    2818  0 17:05 ?        00:00:00 python3 /usr/lib/blueberry/s
agrorec     2935    2927  0 17:05 ?        00:00:00 /usr/sbin/rfkill event
agrorec     2945    2576  0 17:05 ?        00:00:00 /usr/libexec/evolution-addre
agrorec     2958    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfsd-metadata
agrorec     2976    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfs-udisks2-vo
agrorec     2985    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfs-goa-volume
agrorec     2989    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfs-mtp-volume
agrorec     2993    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfs-gphoto2-vo
agrorec     2998    2576  0 17:05 ?        00:00:00 /usr/libexec/gvfs-afc-volume
agrorec     3047    2708  0 17:07 ?        00:00:00 /usr/libexec/gvfsd-trash --s
agrorec     3073    2708  0 17:07 ?        00:00:00 /usr/libexec/gvfsd-network -
agrorec     3085    2708  0 17:07 ?        00:00:00 /usr/libexec/gvfsd-dnssd --s
agrorec     3475    2769  0 17:08 ?        00:00:00 /usr/lib/x86_64-linux-gnu/we
agrorec     4272    2729  0 17:15 ?        00:00:18 xfce4-terminal
agrorec     4277    4272  0 17:15 pts/0    00:00:00 bash
agrorec     7482    2729 19 17:34 ?        00:08:34 /usr/lib/firefox/firefox
agrorec     7564    7482  7 17:34 ?        00:03:20 /usr/lib/firefox/firefox -co
agrorec     7616    7482  0 17:34 ?        00:00:09 /usr/lib/firefox/firefox -co
agrorec     7660    7482  0 17:34 ?        00:00:03 /usr/lib/firefox/firefox -co
agrorec     7704    7482  7 17:34 ?        00:03:23 /usr/lib/firefox/firefox -co
agrorec     7850    7482  0 17:39 ?        00:00:16 /usr/lib/firefox/firefox -co
agrorec     7900    7482  0 17:41 ?        00:00:14 /usr/lib/firefox/firefox -co
agrorec     7935    7482 34 17:41 ?        00:12:39 /usr/lib/firefox/firefox -co
agrorec     8316    7482  0 17:56 ?        00:00:00 /usr/lib/firefox/firefox -co
agrorec     8608    7482  0 18:14 ?        00:00:00 /usr/lib/firefox/firefox -co
agrorec     8724    4277  0 18:18 pts/0    00:00:00 ps -fU agrorec
```

***

## Network

Pour afficher la liste des cartes réseau j'ai utilisé la commande ```lshw -class network```

### Output 

```console
agrorec@agrorec-Aspire-F5-573G:~$ sudo lshw -class network
[sudo] password for agrorec:     
  *-network                 
       description: Wireless interface
       product: QCA9377 802.11ac Wireless Network Adapter
       vendor: Qualcomm Atheros
       physical id: 0
       bus info: pci@0000:03:00.0
       logical name: wlp3s0
       version: 31
       serial: 40:49:0f:80:d6:7f
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress bus_master cap_list ethernet physical wireless
       configuration: broadcast=yes driver=ath10k_pci driverversion=5.4.0-51-generic firmware=WLAN.TF.2.1-00021-QCARMSWP-1 ip=192.168.0.45 latency=0 link=yes multicast=yes wireless=IEEE 802.11
       resources: irq:132 memory:94200000-943fffff
  *-network
       description: Ethernet interface
       product: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
       vendor: Realtek Semiconductor Co., Ltd.
       physical id: 0.1
       bus info: pci@0000:04:00.1
       logical name: enp4s0f1
       version: 12
       serial: 54:ab:3a:b3:20:ce
       capacity: 1Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt-fd autonegotiation
       configuration: autonegotiation=on broadcast=yes driver=r8169 firmware=rtl8411-2_0.0.1 07/08/13 latency=0 link=no multicast=yes port=MII
       resources: irq:19 ioport:3000(size=256) memory:94404000-94404fff memory:94400000-94403fff
```
# 

* La carte WiFi est un composant conçu pour permettre aux ordinateurs de communiquer sur un réseau sans fil. Les adaptateurs réseau sans fil fonctionnent en se connectant à l'ordinateur via des périphériques de carte d'extension ou des cartes conçues pour étendre les foctionnalités de l'ordinateur, telles que des cartes mémoires ou des cartes PC ;
* Le contrôleur Ethernet est un matériel présent à l'intérieur d'un ordinateur qui lui permet de communiquer avec la technologie de réseau filaire appelée Ethernet. Il s'agit généralement d'une carte de circuit imprimé ou d'une partie de la carte mère de l'ordinateur. Elle comprend une connexion au reste de l'ordinateur, des micropuces pour gérer la communication Ethernet et un port Ethernet où un câble Ethernet peut être branché. 

# 

Pour lister tous les ports TCP et UDP en utilisation j'ai utilisé la commande ```less /etc/services
grep -w 80 /etc/services```

### Output

```console
qmqp            628/tcp
ipp             631/tcp                         # Internet Printing Protocol
#
# UNIX specific services
#
exec            512/tcp
biff            512/udp         comsat
login           513/tcp
who             513/udp         whod
shell           514/tcp         cmd syslog      # no passwords used
syslog          514/udp
printer         515/tcp         spooler         # line printer spooler
talk            517/udp
ntalk           518/udp
route           520/udp         router routed   # RIP
gdomap          538/tcp                         # GNUstep distributed objects
gdomap          538/udp
uucp            540/tcp         uucpd           # uucp daemon
klogin          543/tcp                         # Kerberized `rlogin' (v5)
kshell          544/tcp         krcmd           # Kerberized `rsh' (v5)
dhcpv6-client   546/udp
dhcpv6-server   547/udp
afpovertcp      548/tcp                         # AFP over TCP
/etc/services 
``` 

***

## II. Scripting

Pour mes scripts j'ai utilisé le langage natif Bash qui est l'acronyme de Bourne Again Shell. Les scripts Bash permettent de raccourcir les tâches répétitives en un seul appel de fonction. 

Voici mon script qui affiche un résumé de l'OS : 
```sh
#!/bin/bash
#Audrey Flambard
#21/10/2020
#Ce script permet d'obtenir des informations sur la machine ainsi que la liste des utilisateurs et le débit en download et en upload vers Internet. Il calcule aussi le temps de réponse moyen vers 8.8.8.8. 

echo -e "\n"

echo "Get average ping latency to 8.8.8.8 server..."
echo "Get average download  speed..."
echo "Get average upload  speed..."

echo -e "\n"
echo "------------------------"
echo " footprinting report :"
echo "------------------------"
echo "Machine name :" $HOSTNAME

echo "Main IP :" $(hostname -I | cut -f1 -d' ')

echo "OS :" $(cat /etc/os-release | grep 'PRETTY_NAME=' | cut -f2 -d '"' | cut -f1,2 -d " ")

echo "Version :" $(cat /etc/os-release | grep 'VERSION=' | cut -f2 -d '"')

echo "Linux kernel :" $(uname -r)

echo "Up since :" $(who -b | cut -d' ' -f13-14)

echo "Is OS up-to-date ?"
ver="$(cat /etc/os-release | grep 'VERSION=' | cut -f2 -d '"')"
latest_ver="20 (Ulyana)"
if [ "$ver" == "$latest_ver" ] ; then
        echo "true"
else
        echo "false"
fi

echo -e "\n"
echo "RAM
   Used :" $(free -h | grep Mem | cut -f18 -d " ")
echo "   Free :" $(free -h | grep Mem | cut -f25 -d " ")

echo "Disk
   Used :" $(df -H /dev/sda1 | grep /dev/sda1 | cut -f11 -d' ')
   echo "   Free :" $(df -H /dev/sda1 | grep /dev/sda1 | cut -f13 -d " ")

echo -e "\n"

echo "Users list :
" $(cut -d: -f1 /etc/passwd)

echo -e "\n"

echo "- 8.8.8.8 average ping time :" $(ping -c 4 8.8.8.8 | tail -1 | awk '{print $4}' | cut -d '/' -f2)"ms"
echo "- average download speed :" $(curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 - | grep Download: | cut -f 2,3 -d " ")
echo "- average upload speed :" $(curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 - | grep Upload: | cut -f 2,3 -d " " )
```

# 

Je lui donne les permissions d'exécution avec la commande : ```chmod 770 footprinting.sh```

### Output 

```console
agrorec@agrorec-Aspire-F5-573G:~/Desktop/$ ./footprinting.sh


Get average ping latency to 8.8.8.8 server...
Get average download  speed...
Get average upload  speed...


------------------------
 footprinting report :
------------------------
Machine name : agrorec-Aspire-F5-573G
Main IP : 192.168.0.45
OS : Linux Mint
Version : 20 (Ulyana)
Linux kernel : 5.4.0-52-generic
Up since : 2020-11-09 09:34
Is OS up-to-date ?
true


RAM
   Used : 2,1Gi
   Free : 3,3Gi
Disk
   Used : 40G
   Free : 199G


Users list :
 root daemon bin sys sync games man lp mail news uucp proxy www-data backup list irc gnats nobody systemd-network systemd-resolve syslog messagebus _apt uuidd cups-pk-helper kernoops rtkit avahi-autoipd usbmux systemd-coredump ntp lightdm dnsmasq saned nm-openvpn avahi colord speech-dispatcher pulse hplip geoclue agrorec tcpdump tss _flatpak systemd-timesync nvidia-persistenced _rpc statd sshd


- 8.8.8.8 average ping time : 37.235ms
- average download speed : 10.09 Mbit/s
- average upload speed : 1.32 Mbit/s
```

#  

Le deuxième script permet, en fonction d'arguments qui lui sont passés, de lock l'écran après x secondes ou d'éteindre le PC après x secondes : 

```sh
#!/bin/bash
#Flambard Audrey

#Verrouille le pc après x secondes
if [ $1 = "lock" ]
then
        sleep $2;xdg-screensaver lock
#Eteins le pc après x secondes
elif [ $1 = "shutdown" ]
then
        sleep $2;shutdown now
else
        echo "input incorrect"
fi
```

***

## III. Gestion de softs


### Le gestionnaire de paquets

Un **gestionnaire de paquets** est un système qui permet d'installer des logiciels, de les maintenir à jour et de les désinstaller. Son travail est de n'utiliser que des éléments compatibles entre eux.
Un **paquet** est une archive de fichiers informatiques, d'informations et de procédures nécessaires à l'installation d'un logiciel sur un système d'exploitation en s'assurant de la cohérence fonctionnelle du système modifié.
Le gestionnaire de paquets permet d'effectuer différentes opérations sur les paquets disponibles : 
* Installation, mise à jour, et désinstallation ;
* Utilisation des paquets provenant de supports variés (CD d'installation, dépôts sur internet, partage réseau ...) ;
* Vérification de la compatibilité 
* Vérification des dépendances logicielles afin d'en obtenir une version fonctionnelle.

# 

L'intérêt d'utiliser un gestionnaire de paquets par rapport au téléchargement en direct sur Internet est que les paquets seront automatiquement compatibles avec l'OS. S'ils ne le sont pas alors le gestionnaire va faire en sorte de trouver les paquets manquants pour remédier à cela. 
De plus, le gestionnaire nous procure une sécurité du téléchargement puisque les logiciels sont centralisés sur un serveur de confiance.

# 

Pour lister tous les paquets déjà installés j'utilise la commande ```apt list```

### Output

``` console
[...]
yotta-doc/focal,focal 0.20.0-1 all
yotta/focal,focal 0.20.0-1 all
youker-assistant/focal 3.0.2-0ubuntu1 amd64
youtube-dl/focal,focal 2020.03.24-1 all
yowsup-cli/focal,focal 3.2.3-1 all
yp-tools/focal 3.3-5.3 amd64
yrmcds/focal 1.1.8-1.1build1 amd64
ytcc/focal,focal 1.8.1-1 all
ytnef-tools/focal 1.9.3-1 amd64
ytree/focal 1.99pl1-2 amd64
yubico-piv-tool/focal 2.0.0-2 amd64
yubikey-luks/focal,focal 0.5.1+29.g5df2b95-3 all
yubikey-manager/focal,focal 3.1.1-1 all
yubikey-personalization-gui/focal 3.1.24-1build1 amd64
yubikey-personalization/focal 1.20.0-2 amd64
yubikey-server-c/focal 0.5-1.1 amd64
yubioath-desktop/focal 5.0.3-1 amd64
yubiserver/focal 0.6-3.1 amd64
yudit-common/focal,focal 2.9.6-8build1 all
yudit-doc/focal,focal 2.9.6-8build1 all
yudit/focal 2.9.6-8build1 amd64
yui-compressor/focal,focal 2.4.8-2 all
yuyo-gtk-theme/focal,focal 0.3 all
yydebug/focal,focal 1.1.0-11 all
z-push-backend-caldav/focal,focal 2.5.1-1 all
z-push-backend-carddav/focal,focal 2.5.1-1 all
z-push-backend-combined/focal,focal 2.5.1-1 all
z-push-backend-galsearch-ldap/focal,focal 2.5.1-1 all
z-push-backend-imap/focal,focal 2.5.1-1 all
z-push-backend-kopano/focal,focal 2.5.1-1 all
z-push-backend-ldap/focal,focal 2.5.1-1 all
z-push-common/focal,focal 2.5.1-1 all
z-push-ipc-memcached/focal,focal 2.5.1-1 all
z-push-kopano-gab2contacts/focal,focal 2.5.1-1 all
z-push-kopano-gabsync/focal,focal 2.5.1-1 all
z-push-state-sql/focal,focal 2.5.1-1 all
z-push/focal,focal 2.5.1-1 all
z3/focal 4.8.7-4build1 amd64
z3/focal 4.8.7-4build1 i386
z80asm/focal 1.8-1build1 amd64
z80dasm/focal 1.1.5-1 amd64
z8530-utils2/focal 3.0-1-10 amd64
z88-data/focal,focal 13.0.0+dfsg2-6 all
z88-doc/focal,focal 13.0.0+dfsg2-6 all
z88/focal 13.0.0+dfsg2-6 amd64
zabbix-agent/focal 1:4.0.17+dfsg-1 amd64
zabbix-frontend-php/focal,focal 1:4.0.17+dfsg-1 all
zabbix-java-gateway/focal,focal 1:4.0.17+dfsg-1 all
zabbix-proxy-mysql/focal 1:4.0.17+dfsg-1 amd64
zabbix-proxy-pgsql/focal 1:4.0.17+dfsg-1 amd64
zabbix-proxy-sqlite3/focal 1:4.0.17+dfsg-1 amd64
zabbix-server-mysql/focal 1:4.0.17+dfsg-1 amd64
zabbix-server-pgsql/focal 1:4.0.17+dfsg-1 amd64
zalign/focal 0.9.1-4build1 amd64
zam-plugins/focal 3.9~repack3-1build1 amd64
zangband-data/focal,focal 1:2.7.5pre1-12 all
zangband/focal 1:2.7.5pre1-12 amd64
zanshin/focal 0.5.71-1build2 amd64
zaqar-common/focal-updates,focal-updates 10.0.0-0ubuntu0.20.04.1 all
zaqar-server/focal-updates,focal-updates 10.0.0-0ubuntu0.20.04.1 all
zatacka/focal 0.1.8-5.2build1 amd64
zathura-cb/focal 0.1.8-2build1 amd64
zathura-dev/focal 0.4.5-2 amd64
zathura-djvu/focal 0.2.9-1 amd64
zathura-pdf-poppler/focal 0.3.0-1 amd64
zathura-ps/focal 0.2.6-1build1 amd64
zathura/focal 0.4.5-2 amd64
zaz-data/focal,focal 1.0.0~dfsg1-5build1 all
zaz/focal 1.0.0~dfsg1-5build1 amd64
zbackup/focal 1.4.4-3build3 amd64
zbar-tools/focal 0.23-1.3 amd64
zbar-tools/focal 0.23-1.3 i386
zbarcam-gtk/focal 0.23-1.3 amd64
zbarcam-gtk/focal 0.23-1.3 i386
zbarcam-qt/focal 0.23-1.3 amd64
zbarcam-qt/focal 0.23-1.3 i386
zdbsp/focal 1.19+20181027+dfsg.1-2build1 amd64
zeal/focal 1:0.6.1-1build1 amd64
zec/focal,focal 0.12-5 all
zegrapher/focal 3.0.2-1build1 amd64
zeitgeist-core/focal 1.0.2-3ubuntu2 amd64
zeitgeist-datahub/focal 1.0.2-3ubuntu2 amd64
zeitgeist/focal,focal 1.0.2-3ubuntu2 all
zemberek-java-demo/focal,focal 2.1.1-8.2 all
zemberek-server/focal,focal 0.7.1-12.2 all
zenity-common/focal,focal,now 3.32.0-5 all [installed,automatic]
zenity/focal,now 3.32.0-5 amd64 [installed,automatic]
zenlisp/focal 2013.11.22-2build1 amd64
zephyr-clients/focal 3.1.2-1build3 amd64
zephyr-server-krb5/focal 3.1.2-1build3 amd64
zephyr-server/focal 3.1.2-1build3 amd64
zeroc-glacier2/focal 3.7.3-1build2 amd64
zeroc-ice-all-dev/focal,focal 3.7.3-1build2 all
zeroc-ice-all-runtime/focal,focal 3.7.3-1build2 all
zeroc-ice-compilers/focal 3.7.3-1build2 amd64
zeroc-ice-manual/focal,focal 3.5.0-1 all
zeroc-ice-slice/focal,focal 3.7.3-1build2 all
zeroc-ice-utils-java/focal,focal 3.7.3-1build2 all
zeroc-ice-utils/focal 3.7.3-1build2 amd64
zeroc-icebox/focal 3.7.3-1build2 amd64
zeroc-icebridge/focal 3.7.3-1build2 amd64
zeroc-icegrid/focal 3.7.3-1build2 amd64
zeroc-icegridgui/focal,focal 3.7.3-1build2 all
zeroc-icepatch2/focal 3.7.3-1build2 amd64
zerofree/focal 1.1.1-1 amd64
zerofree/focal 1.1.1-1 i386
zfcp-hbaapi-dev/focal 2.2.0-0ubuntu1 amd64
zfcp-hbaapi-utils/focal 2.2.0-0ubuntu1 amd64
zfs-auto-snapshot/focal,focal 1.2.4-2 all
zfs-dkms/focal-updates,focal-updates 0.8.3-1ubuntu12.4 all
zfs-dracut/focal-updates 0.8.3-1ubuntu12.4 amd64
zfs-fuse/focal 0.7.0-20 amd64
zfs-initramfs/focal-updates 0.8.3-1ubuntu12.4 amd64
zfs-test/focal-updates 0.8.3-1ubuntu12.4 amd64
zfs-zed/focal-updates 0.8.3-1ubuntu12.4 amd64
zfsnap/focal,focal 1.11.1-5.1 all
zfsutils-linux/focal-updates 0.8.3-1ubuntu12.4 amd64
zftp/focal 20061220+dfsg3-4.4build1 amd64
zgen/focal,focal 0~20150919-3 all
zh-autoconvert/focal 0.3.16-5 amd64
zhcon-data/focal,focal 1:0.2.6-16build1 all
zhcon/focal 1:0.2.6-16build1 amd64
zile/focal 2.4.14-7build1 amd64
zim/focal,focal 0.72.0-1 all
zimpl/focal 3.3.6-2 amd64
zimwriterfs/focal 1.2-1build4 amd64
zinnia-utils/focal 0.06-6ubuntu2 amd64
zip/focal,now 3.0-11build1 amd64 [installed,automatic]
zip/focal 3.0-11build1 i386
zipalign/focal 1:8.1.0+r23-3ubuntu2 amd64
zipcmp/focal 1.5.1-0ubuntu1 amd64
zipcmp/focal 1.5.1-0ubuntu1 i386
zipmerge/focal 1.5.1-0ubuntu1 amd64
zipmerge/focal 1.5.1-0ubuntu1 i386
zipper.app/focal 1.5-3build1 amd64
ziproxy/focal 3.3.1-2.1 amd64
ziptime/focal 1:8.1.0+r23-3ubuntu2 amd64
ziptool/focal 1.5.1-0ubuntu1 amd64
ziptool/focal 1.5.1-0ubuntu1 i386
zita-ajbridge/focal-updates 0.8.4-1ubuntu0.20.04.1 amd64
zita-alsa-pcmi-utils/focal 0.3.2-1 amd64
zita-alsa-pcmi-utils/focal 0.3.2-1 i386
zita-at1/focal 0.6.2-1build1 amd64
zita-bls1/focal 0.3.3-1 amd64
zita-dc1/focal 0.3.3-1 amd64
zita-dpl1/focal 0.3.3-1 amd64
zita-lrx/focal 0.1.2-1build1 amd64
zita-mu1/focal 0.3.3-1build1 amd64
zita-njbridge/focal 0.4.4-1 amd64
zita-resampler/focal 1.6.2-1 amd64
zita-resampler/focal 1.6.2-1 i386
zita-rev1/focal 0.2.2-1build1 amd64
zkg/focal,focal 2.0.7-1 all
zktop/focal,focal 1.0.0-3 all
zlib-gst/focal 3.2.5-1.3build3 amd64
zlib-gst/focal 3.2.5-1.3build3 i386
zlib1g-dev/focal-updates 1:1.2.11.dfsg-2ubuntu1.1 amd64
zlib1g-dev/focal-updates 1:1.2.11.dfsg-2ubuntu1.1 i386
zlib1g/focal-updates,now 1:1.2.11.dfsg-2ubuntu1.1 amd64 [installed,automatic]
zlib1g/focal-updates 1:1.2.11.dfsg-2ubuntu1.1 i386
zlibc/focal 0.9k-4.3 amd64
zmakebas/focal 1.2-1.1build1 amd64
zmap/focal 2.1.1-2build3 amd64
zmf2epub/focal 0.9.6-2build1 amd64
zmf2odg/focal 0.9.6-2build1 amd64
znc-backlog/focal 0.20180824+1.7.5-4 amd64
znc-dev/focal 1.7.5-4 amd64
znc-perl/focal 1.7.5-4 amd64
znc-push/focal 1.0.0+git20190521.78d0385+1.7.5-4 amd64
znc-python/focal 1.7.5-4 amd64
znc-tcl/focal 1.7.5-4 amd64
znc/focal 1.7.5-4 amd64
zoem/focal 11-166-1.2 amd64
zomg/focal 0.8.1-2 amd64
zonemaster-cli/focal,focal 1.0.5-1 all
zoneminder-doc/focal,focal 1.32.3-2ubuntu2 all
zoneminder/focal 1.32.3-2ubuntu2 amd64
zookeeper-bin/focal 3.4.13-5build1 amd64
zookeeper/focal,focal 3.4.13-5build1 all
zookeeperd/focal,focal 3.4.13-5build1 all
zoom-player/focal 1.1.5~dfsg-5 amd64
zopfli/focal 1.0.3-1build1 amd64
zoph/focal,focal 0.9.11-2 all
zpaq/focal 7.15-1build1 amd64
zplug/focal,focal 2.4.2-1 all
zpspell/focal 0.4.3-4.1build2 amd64
zram-config/focal,focal 0.5 all
zram-tools/focal,focal 0.3.2.1-1 all
zsh-antigen/focal,focal 2.2.3-2 all
zsh-autosuggestions/focal,focal 0.6.4-1 all
zsh-common/focal,focal,now 5.8-3ubuntu1 all [installed,automatic]
[...]
``` 

# 

Pour déterminer la provenance des paquets j'utilise la commande ``` apt-cache policy nomdufichier``` 

### Output

Exemple : 

```
$ apt-cache policy zsh       
zsh:
  Installed: 5.8-3ubuntu1
  Candidate: 5.8-3ubuntu1
  Version table:
 *** 5.8-3ubuntu1 500
        500 http://fr.archive.ubuntu.com/ubuntu focal/main amd64 Packages
        100 /var/lib/dpkg/status
``` 

***

## IV. Machine virtuelle

### Partage de fichiers

Etant sous Linux, j'ai utilisé le partage NFS entre ma machine Linux Mint 20 et ma VM CentOS 7.

Voici les étapes que j'ai suivies :

**Machine serveur Linux Mint :**

Update package
```
sudo apt update 
sudo apt upgrade
sudo reboot
```
Installation NFS
``` 
agrorec@agrorec-Aspire-F5-573G:/$ sudo apt install nfs-kernel-server
```

Création d'un dossier partagé
```
agrorec@agrorec-Aspire-F5-573G:/$ sudo mkdir -p /mnt/nfs_share
```

Suppression de toutes les restrictions de permissions afin de donner entièrement accès à la machine client 
```
agrorec@agrorec-Aspire-F5-573G:/$ sudo chown -R nobody:nogroup /mnt/nfs_share
agrorec@agrorec-Aspire-F5-573G:/$ sudo chmod 777 /mnt/nfs_share 
```

Exportation du dossier partagé
```
agrorec@agrorec-Aspire-F5-573G:/$ sudo exportfs -a
agrorec@agrorec-Aspire-F5-573G:/$ sudo systemctl restart nfs-kernel-server
```

**Machine client CentOS**

Connexion SSH 
```
agrorec@agrorec-Aspire-F5-573G:/$ ssh root@192.168.120.51
The authenticity of host '192.168.120.51 (192.168.120.51)' can't be established.
ECDSA key fingerprint is SHA256:kc0hDig4G3Vi1wXAU9qf9AZuYpsumGVF6XhRx1vo2uY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '192.168.120.51' (ECDSA) to the list of known hosts.
root@192.168.120.51's password: 
Last login: Mon Nov  9 09:39:32 2020 from localhost
```
Installation NFS
```
[root@localhost ~]# sudo yum update
[root@localhost ~]# sudo yum install nfs-utils
```

Création d'un dossier mount point
```
[root@localhost ~]# sudo mkdir -p /mnt/nfs_shareclient
```

Mount le NFS partagé dans le dossier créé et création d'un fichier test1
```
[root@localhost ~]# mount 192.168.120.1:/mnt/nfs_share /mnt/nfs_shareclient
[root@localhost ~]# df -h
Filesystem                    Size  Used Avail Use% Mounted on
devtmpfs                      484M     0  484M   0% /dev
tmpfs                         496M     0  496M   0% /dev/shm
tmpfs                         496M  6,8M  489M   2% /run
tmpfs                         496M     0  496M   0% /sys/fs/cgroup
/dev/mapper/centos-root       6,2G  1,5G  4,8G  24% /
/dev/sda1                    1014M  167M  848M  17% /boot
tmpfs                         100M     0  100M   0% /run/user/0
192.168.120.1:/mnt/nfs_share  234G   38G  185G  17% /mnt/nfs_shareclient
[root@localhost ~]# cd /mnt/nfs_shareclient
[root@localhost nfs_shareclient]# touch test1
```

Le fichier est bien présent sur mon pc : 

```
agrorec@agrorec-Aspire-F5-573G:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            3,8G     0  3,8G   0% /dev
tmpfs           784M  1,6M  783M   1% /run
/dev/sda1       234G   38G  185G  17% /
tmpfs           3,9G     0  3,9G   0% /dev/shm
tmpfs           5,0M  4,0K  5,0M   1% /run/lock
tmpfs           3,9G     0  3,9G   0% /sys/fs/cgroup
tmpfs           784M   56K  784M   1% /run/user/1000
agrorec@agrorec-Aspire-F5-573G:~$ ls /mnt/nfs_share
test1  test1.txt
agrorec@agrorec-Aspire-F5-573G:~$ ls /mnt/nfs_share -al
total 8
drwxrwxrwx 2 nobody  nogroup 4096 Nov  9 11:26 .
drwxr-xr-x 3 root    root    4096 Nov  6 18:51 ..
-rw-r--r-- 1 nobody  nogroup    0 Nov  9 11:26 test1
```
***


![reference link](https://media.giphy.com/media/26u42gAFROFb0VyCI/giphy.gif)

